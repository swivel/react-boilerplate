import styled from 'styled-components';

const MainStyled = styled.div`
	background: #2C4058;
	color: #FFF;
`;

export default MainStyled;
